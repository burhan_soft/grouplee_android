package com.burhan.soft.custom.control;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.burhan.soft.R;

/**
 * Created by TAYEBT on 4/18/2015.
 */
public class BurhanGridView extends android.widget.GridView implements AbsListView.OnScrollListener, android.widget.AdapterView.OnItemClickListener
{
    private int mScrollOfsset;
    private int initialTopPadding=0;
    private int mDisplayWidth = 0;
    private int headerViewHeight = 0;
    private ListAdapter originalAdapter;
    private BaseAdapter fakeAdapter;
    private int lastPos=0;
    private OnScrollListener scrollListenerFromActivity;
    private OnItemClickListener clickListenerFromActivity;
    private ListView.FixedViewInfo mHeaderViewInfo;
    private Boolean setFixed=false;
    private Boolean bringToFront=true;
    private int verticalSpacing=0;

    private final float THRESHOLD = 400;
    private LayoutInflater inflater;
    View headerView;
    LinearLayout ll;
    private boolean ENABLED_HEADER_VIEW = false;

    public BurhanGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public BurhanGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public BurhanGridView(Context context) {
        super(context);
        init(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private void init(Context context)
    {
        super.setOnScrollListener(this);
    }

    @Override
    public void onDetachedFromWindow()
    {
        if ( originalAdapter != null )
        {
            originalAdapter.unregisterDataSetObserver( originalAdapterDataSetObserver );
        }
        super.onDetachedFromWindow();
    }

    @Override
    public void setVerticalSpacing( int spacing )
    {
        this.verticalSpacing = spacing;
        super.setVerticalSpacing(spacing);
    }

    @Override
    public void setOnItemClickListener( OnItemClickListener l )
    {
        clickListenerFromActivity = l;
        super.setOnItemClickListener(this);
    }

    @Override
    public void setOnScrollListener( OnScrollListener l )
    {
        scrollListenerFromActivity = l;
        //scroll Listener
        super.setOnScrollListener(this);
    }

    @Override
    public void setAdapter( ListAdapter a )
    {
        originalAdapter = a;
        fakeAdapter = new mListAdapter();
        //
        originalAdapter.registerDataSetObserver( originalAdapterDataSetObserver );
        super.setAdapter(fakeAdapter);
    }

    DataSetObserver originalAdapterDataSetObserver = new DataSetObserver()
    {
        @Override
        public void onChanged()
        {
            fakeAdapter.notifyDataSetChanged();
        }

        @Override
        public void onInvalidated()
        {
            fakeAdapter.notifyDataSetInvalidated();
        }
    };

    /**
     *
     *
     *
     */
    public class mListAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return originalAdapter.getCount() > 0 ? originalAdapter.getCount() + BurhanGridView.this.getNumColumnsCompat() : 0;
        }

        @Override
        public Object getItem(int position)
        {
            return originalAdapter.getItem(position + BurhanGridView.this.getNumColumnsCompat());
        }

        @Override
        public long getItemId(int position)
        {
            return originalAdapter.getItemId(position + BurhanGridView.this.getNumColumnsCompat());
        }

        class InternalViewHolder
        {
            View view;
        }

        @Override
        public int getViewTypeCount()
        {
            return 2;
        }

        @Override
        public int getItemViewType( int position )
        {
            if ( position < BurhanGridView.this.getNumColumnsCompat() )
            {
                return IGNORE_ITEM_VIEW_TYPE;
            }else{
                return 1;
            }
        }

        @Override
        public View getView(int position, View convert, ViewGroup parent)
        {
            if ( position < BurhanGridView.this.getNumColumnsCompat() )
            {
                if ( convert == null )
                {
                    convert = new LinearLayout( getContext() );
                    ViewGroup.LayoutParams mParams = new LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, headerViewHeight );
                    mParams.height = headerViewHeight;
                    convert.setLayoutParams( mParams );
                    convert.setVisibility(View.INVISIBLE);
                }
            }else{
                int realPosition = position - BurhanGridView.this.getNumColumnsCompat();
                convert = originalAdapter.getView(realPosition, convert, parent);
            }
            return convert;
        }
    };

    /**
     * add header layout
     * @param v View
     * @param data extra data
     * @param isSelectable boolean
     */
    public void addHeaderView( final View v, final Object data, final boolean isSelectable)
    {
        mHeaderViewInfo = new ListView(getContext()).new FixedViewInfo();
        mHeaderViewInfo.view = v;
        mHeaderViewInfo.data = data;
        mHeaderViewInfo.isSelectable = isSelectable;

        setupView(v);

        int topPadding = getPaddingTop();
        if(initialTopPadding == 0){
            initialTopPadding = topPadding;
        }
        headerViewHeight = v.getMeasuredHeight();

        RelativeLayout parent = (RelativeLayout)getParent();
        parent.addView(v, 0);
        if ( bringToFront )
        {
            v.bringToFront();
        }
    }

    public ListView.FixedViewInfo getHeaderView()
    {
        return mHeaderViewInfo;
    }

    private void setupView(View v)
    {
        boolean isLayedOut = !((v.getRight()==0) && (v.getLeft()==0) && (v.getTop()==0) && (v.getBottom()==0));

        if(v.getMeasuredHeight() != 0 && isLayedOut ) return;

        if(mDisplayWidth == 0){
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            mDisplayWidth = displaymetrics.widthPixels;

        }
        v.setLayoutParams(new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        v.measure(MeasureSpec.makeMeasureSpec(mDisplayWidth, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
        v.layout(0, getTotalHeaderHeight(), v.getMeasuredWidth(), getTotalHeaderHeight() + v.getMeasuredHeight());
    }

    /**
     * return header layout
     * @param v layout
     */
    public void addHeaderView(View v) {
        this.addHeaderView(v, null, false);
    }

    /**
     *
     * @return boolean
     */
    public Boolean isInFront()
    {
        return bringToFront;
    }

    /**
     *
     */
    public void setIsInFront( Boolean isInFront )
    {
        bringToFront = isInFront;
    }

    /**
     * set fixed header
     */
    public void setFixedHeader( Boolean fixed )
    {
        this.setFixed = fixed;
    }

    private void drawHeaders()
    {
        if ( mHeaderViewInfo != null )
        {
            int startPos = -mScrollOfsset;
            //
            if ( lastPos != startPos && !setFixed && bringToFront)
            {
                if ( mScrollOfsset <= headerViewHeight )
                {
                    RelativeLayout.LayoutParams mParams = (android.widget.RelativeLayout.LayoutParams)mHeaderViewInfo.view.getLayoutParams();
                    mParams.topMargin = startPos;
                    mHeaderViewInfo.view.setLayoutParams(mParams);
                    mHeaderViewInfo.view.setVisibility( View.VISIBLE );
                }else{
                    mHeaderViewInfo.view.setVisibility( View.GONE );
                }
            }
            lastPos = startPos;
        }
    }
    @Override
    protected void onDraw(Canvas canvas)
    {
        if ( fakeAdapter != null )
        {
            drawHeaders();
        }
        super.onDraw(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

    }

    private int getTotalHeaderHeight()
    {
        return headerViewHeight;
    }

    private int getNumColumnsCompat()
    {
        if (Build.VERSION.SDK_INT >= 11) {
            return getNumColumnsCompat11();

        } else {
            int columns = 0;
            int children = getChildCount();
            if (children > 0) {
                int width = getChildAt(0).getMeasuredWidth();
                if (width > 0) {
                    columns = getWidth() / width;
                }
            }
            return columns > 0 ? columns : AUTO_FIT;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private int getNumColumnsCompat11() {
        return getNumColumns();
    }

    private int getVerticalSpacingCompat()
    {
        if (Build.VERSION.SDK_INT >= 16) {
            return getVerticalSpacingCompat16();
        }else{
            return this.verticalSpacing;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private int getVerticalSpacingCompat16() {
        return getVerticalSpacing();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
    {
        if(this.getAdapter()!=null){
            int count  = this.getChildCount();
            int totalHeaderHeight = getTotalHeaderHeight();

            if (count > this.getNumColumnsCompat()) {
                View child = this.getChildAt(this.getNumColumnsCompat());
                if (child != null) {
                    mScrollOfsset = ((firstVisibleItem / this.getNumColumnsCompat()) * child.getMeasuredHeight()) + totalHeaderHeight - child.getTop()
                            + this.getVerticalSpacingCompat();
                }
            }
        }
        if ( scrollListenerFromActivity != null )
        {
            scrollListenerFromActivity.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }

    }

    /**
     * remove header
     */
    public void removeHeaderView()
    {
        if (mHeaderViewInfo != null)
        {
            RelativeLayout parent = (RelativeLayout)this.getParent();
            parent.removeView( mHeaderViewInfo.view );
            super.setAdapter(originalAdapter);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState)
    {
        if ( scrollListenerFromActivity != null )
        {
            scrollListenerFromActivity.onScrollStateChanged(view, scrollState);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id)
    {
        if ( clickListenerFromActivity != null )
        {
            clickListenerFromActivity.onItemClick(adapter, view, position - BurhanGridView.this.getNumColumnsCompat(), id);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        float startY = ev.getY();

        switch (ev.getAction()){

            case MotionEvent.ACTION_DOWN:

                break;

            case MotionEvent.ACTION_MOVE:

//                if(startY > THRESHOLD && !ENABLED_HEADER_VIEW){
//                    headerView = inflater.inflate(R.layout.refresh_header_view,null);
//
//                    ll =  (LinearLayout) headerView.findViewById(R.id.headerlayout);
//                    ll.setVisibility(View.VISIBLE);
//                    Button btnCancel = (Button) headerView.findViewById(R.id.btnCancel);
//                    btnCancel.setOnClickListener(new OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            removeHeaderView();
//                            ENABLED_HEADER_VIEW = false;
//                        }
//                    });
//                    addHeaderView(headerView,null,true);
//                    ENABLED_HEADER_VIEW = true;
//                }


                break;

            case MotionEvent.ACTION_UP:


                break;
        }

        return super.onTouchEvent(ev);
    }


}