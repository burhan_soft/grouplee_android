package com.burhan.soft.custom.control;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.burhan.soft.R;

/**
 * Created by sandip.mahajan on 3/22/2015.
 */
public class BurhanTextView extends TextView {

    public BurhanTextView(Context context) {
        super(context);
    }

    public BurhanTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public BurhanTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //intercept Typeface change and set it with our custom font
    @Override
    public void setTypeface(Typeface tf) {
        if (tf.getStyle() == Typeface.BOLD) {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/gothic_bold.ttf"));
        } else {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/gothic.ttf"));
        }
        this.setTextSize(TypedValue.COMPLEX_UNIT_PT, getResources().getInteger(R.integer.text_font_size));
        this.setTextColor(getResources().getColor(R.color.text_color));
    }
}
