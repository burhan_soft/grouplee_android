package com.burhan.soft.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.burhan.soft.ui.IBaseActivity;
import com.burhan.soft.ui.MainActivity;
import com.burhan.soft.ui.MenuMainActivity;

import java.lang.ref.WeakReference;

/**
 * Created by sandip.mahajan on 3/24/2015.
 * This class will consist of all Utility methods
 */
public class UIUtil {

    private static ProgressDialog progress;

    public static MenuMainActivity menuMainActivity;
    public static MainActivity mainActivity;



    /**
     * This will register handler for each activity
     *
     * @param context
     */
    public static Handler registerWeakHandler(IBaseActivity context) {
        return new UIhandler(context);
    }

    public static void showProgress(Context context) {
        progress = new ProgressDialog(context);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.show();
    }

    public static void hideProgress() {
        if (null != progress) {
            progress.dismiss();
            progress = null;
        }
    }

    /**
     * Weak reference handler to remove handler leaks
     * Created by sandip.mahajan on 3/24/2015.
     */
    private static class UIhandler extends Handler {
        private final WeakReference<IBaseActivity> reference;

        public UIhandler(IBaseActivity activity) {
            reference = new WeakReference<IBaseActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            IBaseActivity activity = reference.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }
}
