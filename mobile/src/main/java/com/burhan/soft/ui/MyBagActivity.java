package com.burhan.soft.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.burhan.soft.R;
import com.burhan.soft.adapter.MybagInnerAdapter;
import com.burhan.soft.custom.control.HorizontalListView;
import com.burhan.soft.data.MybagItem;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardListView;


public class MyBagActivity extends BaseActivity {

    CardListView listView;
    ArrayList<MybagItem> items;
    MybagItem item;

    ImageListCard card;
    CardHeader header;
    CardArrayAdapter mCardArrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

        //Only for test scope, use images on assets folder
        String[] fileName1 = {
                "file:///android_asset/mybag/nike_one.png",
                "file:///android_asset/mybag/nike_two.png",
                "file:///android_asset/mybag/nike_one.png"};
        String[] fileName2 = {
                "file:///android_asset/mybag/mcd_one.png",
                "file:///android_asset/mybag/mcd_two.png",
                "file:///android_asset/mybag/mcd_one.png"};
        String[] fileName3 = {
                "file:///android_asset/mybag/hyundai_one.png",
                "file:///android_asset/mybag/hyundai_two.png",
                "file:///android_asset/mybag/hyundai_one.png"};


        items = new ArrayList<MybagItem>();

        item = new MybagItem("Fashion and Accessories",fileName1);
        items.add(item);

        item = new MybagItem("Fast Food and Dine in",fileName2);
        items.add(item);

        item = new MybagItem("Automobiles",fileName3);
        items.add(item);

        item = new MybagItem("Fashion and Accessories",fileName1);
        items.add(item);

        ArrayList<Card> cards = new ArrayList<Card>();

        for(int i=0;i<items.size();i++) {
            card = new ImageListCard(getApplication(),items.get(i).getTitle(),
                    items.get(i).getImgList());
            card.addCardHeader(header);
            cards.add(card);
        }

        mCardArrayAdapter = new CardArrayAdapter(getApplicationContext(),cards);

        listView = (CardListView) findViewById(R.id.list_cardId);
        //Add an animator
        AnimationAdapter animCardArrayAdapter = new AlphaInAnimationAdapter(mCardArrayAdapter);
        animCardArrayAdapter.setAbsListView(listView);
//        animCardArrayAdapter.setInitialDelayMillis(500);
        if (listView != null) {
            listView.setExternalAdapter(animCardArrayAdapter, mCardArrayAdapter);
        }
    }


    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_mybags;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }


   public class ImageListCard extends Card {

       protected TextView headerView;
       protected HorizontalListView listView;
       String [] imageList;
       MybagInnerAdapter adapter;
       Context context;
       protected String mTitleHeader;
//       protected String mTitleMain;

       public ImageListCard(Context context, String titleHeader, String [] imageList) {
           super(context, R.layout.item_mybag);

           this.context = context;
           this.mTitleHeader = titleHeader;
           this.imageList = imageList;
       }

       @Override
       public void setupInnerViewElements(ViewGroup parent, View view) {
           // TODO Auto-generated method stub

           parent.setBackgroundColor(context.getResources().
                   getColor(R.color.transparent));

           headerView = (TextView) view.findViewById(R.id.textview_item_mybag);
           listView = (HorizontalListView) view
                   .findViewById(R.id.listview_item_mybag);

           headerView.setText(mTitleHeader);
           adapter = new MybagInnerAdapter(getApplicationContext(),imageList);
           listView.setAdapter(adapter);

       }

   }

}
