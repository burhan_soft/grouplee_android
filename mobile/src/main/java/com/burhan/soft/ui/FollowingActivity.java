package com.burhan.soft.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.burhan.soft.R;
import com.burhan.soft.adapter.ExpandableListAdapter;
import com.burhan.soft.data.ExpandableListItem;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardExpand;
import it.gmariotti.cardslib.library.internal.ViewToClickToExpand;
import it.gmariotti.cardslib.library.view.CardListView;

/**
 * Created by TAYEBT on 4/22/2015.
 */
public class FollowingActivity extends Fragment {

    CardArrayAdapter mCardArrayAdapter;
    CardInside card;
    CardListView listView;


    ArrayList<ExpandableListItem> items = new ArrayList<ExpandableListItem>();
    ExpandableListItem item;
    ExpandableListAdapter expandableListAdapter;
    ListView expand_listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.demo_extras_fragment_mix, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initCard();
    }

    public static FollowingActivity newInstance(int page, String title) {
        FollowingActivity fragmentFashion = new FollowingActivity();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFashion.setArguments(args);
        return fragmentFashion;
    }

    /**
     * This method builds a simple cards list
     */
    private void initCard() {
        String [] title = {"Fashion & Accessories","Electronics","Market","Food","Vehicles"};
        String [] item_names = {"Clothings","Jewellery","Cosmetics","Sports","Other"};
        String [] item_images = {"file:///android_asset/category/tweedicon.png",
                "file:///android_asset/category/blingfinder_icon.png",
                "file:///android_asset/category/logo.png",
                "file:///android_asset/category/sport_shoe.png",
                "file:///android_asset/category/logo.png"};



        //Only for test scope, use images on assets folder
        String[] fileName = {"file:///android_asset/category/poster_fashion.png",
                "file:///android_asset/category/electronics.png",
                "file:///android_asset/category/fresh_food.png",
                "file:///android_asset/category/pizza_junk_food.png",
                "file:///android_asset/category/bmw_car_bike.png"};

        //Only for test scope, use 5 different header titles
        int[] resTitleId = {R.string.carddemo_extras_header_expand_area_inside_sea,
                R.string.carddemo_extras_header_expand_area_inside_snow,
                R.string.carddemo_extras_header_expand_area_inside_water,
                R.string.carddemo_extras_header_expand_area_inside_img2,
                R.string.carddemo_extras_header_expand_area_inside_rose};

        //Remove debugging from Picasso
        Picasso.with(getActivity()).setDebugging(false);

        //Init an array of Cards
        ArrayList<Card> cards = new ArrayList<Card>();

            for (int i = 0; i < 5; i++) {

                //Card
                card = new CardInside(getActivity(),fileName[i].toString(),title[i].toString());

                card.setBackgroundResourceId(R.drawable.expand_list_item_bg);

                //Create a CardHeader

                    item = new ExpandableListItem(item_images[i],
                            item_names[i].toString());
                    items.add(item);

                if (i == 0) {

                    //Add an expand area
                    CardExpandInsideSquare expand = new CardExpandInsideSquare(
                            getActivity(),items);
                    card.addCardExpand(expand);


                    //Add a viewToClickExpand to enable click on whole card
                    ViewToClickToExpand viewToClickToExpand =
                            ViewToClickToExpand.builder()
                                    .highlightView(false)
                                    .setupCardElement(ViewToClickToExpand.CardElementUI.CARD);
                    card.setViewToClickToExpand(viewToClickToExpand);


                } else {

                    //Add Header to card
//                    card.addCardHeader(header);

                }

                cards.add(card);
            }

        //Set the arrayAdapter
        mCardArrayAdapter = new CardArrayAdapter(getActivity(), cards);

        listView = (CardListView) getActivity().findViewById(R.id.cardListView);

        //Add an animator
        AnimationAdapter animCardArrayAdapter = new AlphaInAnimationAdapter(mCardArrayAdapter);
        animCardArrayAdapter.setAbsListView(listView);
        //animCardArrayAdapter.setInitialDelayMillis(500);
        if (listView != null) {
            listView.setExternalAdapter(animCardArrayAdapter, mCardArrayAdapter);
        }

    }


    /**
     * Main Card
     */
    public class CardInside extends Card {

        String mFileName;
        String title;
        Context context;

        public CardInside(Context context, String mFileName,String strTitle) {
            super(context, R.layout.layout_cardview);
            this.mFileName = mFileName;
            this.title = strTitle;
            this.context = context;
        }


        @Override
        public void setupInnerViewElements(ViewGroup parent, View view) {
            parent.setBackgroundColor(mContext.getResources().
                    getColor(R.color.transparent));
            ImageView img = (ImageView) view.findViewById(R.id.following_card_headerImageView);
            TextView txt = (TextView) view.findViewById(R.id.following_card_textView);

            txt.setText(title);

            if (img != null) {
                Picasso.with(getContext())
                        .load(Uri.parse(mFileName))
                        .fit()
                        .into(img);
            }

        }
    }

    /**
     * A CardExpand with Texts and other ui elements
     */
    class CardExpandInsideSquare extends CardExpand {

        ArrayList<ExpandableListItem> items;
        Context context;

        public CardExpandInsideSquare(Context context, ArrayList<ExpandableListItem> items) {
            super(context, R.layout.following_card_expand_layout);
            this.items = items;
            this.context = context;
        }



        @Override
        public void setupInnerViewElements(ViewGroup parent, View view) {
            parent.setBackgroundColor(context.getResources().
                    getColor(R.color.transparent));

            expandableListAdapter = new ExpandableListAdapter(context,items);
            expand_listView = (ListView) view.findViewById(R.id.expand_listview);
            expand_listView.setAdapter(expandableListAdapter);

        }
    }
}