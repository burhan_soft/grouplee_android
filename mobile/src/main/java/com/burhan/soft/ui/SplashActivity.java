package com.burhan.soft.ui;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.burhan.soft.R;
import com.burhan.soft.util.UIUtil;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class SplashActivity extends LoginActionBaseActivity implements IBaseActivity {

    private Handler handler;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            startApp();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        handler = UIUtil.registerWeakHandler(this);
        handler.postDelayed(runnable, 5 * 1000);
        TransitionDrawable transition = (TransitionDrawable) getResources().getDrawable(R.drawable.expand_collapse);
        ImageView logo = (ImageView) findViewById(R.id.splash_screen_logo);
        logo.setImageDrawable(transition);
        transition.startTransition(3000);

//        printKeyHash(SplashActivity.this);

        final UserFunctions userFunctions = new UserFunctions();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
               JSONObject obj = userFunctions.registerUser("sandip", "", "");
            }
        });thread.start();


    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_splash;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }

    private void startApp() {
        if (isLoggedIn) {
            navigateUI(MenuMainActivity.class);
        } else {
            navigateUI(MainActivity.class);
        }
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

}
