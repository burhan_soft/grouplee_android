package com.burhan.soft.ui;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.burhan.soft.R;
import com.burhan.soft.custom.control.BurhanTextView;
import com.burhan.soft.custom.control.SquareImageView;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardGridArrayAdapter;
import it.gmariotti.cardslib.library.view.CardGridView;

/**
 * Created by TAYEBT on 4/27/2015.
 */
public class FeedActivity extends ActionBarActivity {

    //    private CardArrayAdapter
    private CardGridArrayAdapter mCardArrayAdapter;
    //    private CardRecyclerView mRecyclerView;
    private CardGridView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_activity);


        //Load cards
        new LoaderAsyncTask().execute();
    }

    private ArrayList<Card> initCard() {

        //Only for test scope, use images on assets folder
        String[] fileName = {"file:///android_asset/feed/kfc_flyers.png",
                "file:///android_asset/feed/mcdonald_flyers.png",
                "file:///android_asset/images/water.jpg",
                "file:///android_asset/images/img2.jpg"};



        ArrayList<Card> cards = new ArrayList<Card>();

        for (int i = 0; i < 4; i++) {


            MaterialCard card = new MaterialCard(getApplicationContext(),
                    fileName[i].toString());


            cards.add(card);

        }

        return cards;
    }

    private void updateAdapter(ArrayList<Card> cards) {
        if (cards != null) {
            //Set the arrayAdapter
            mCardArrayAdapter = new CardGridArrayAdapter(getApplicationContext(), cards);

            listView = (CardGridView) findViewById(R.id.feed_cardListView);

            //Add an animator
            AnimationAdapter animCardArrayAdapter = new AlphaInAnimationAdapter(mCardArrayAdapter);
            animCardArrayAdapter.setAbsListView(listView);
            //animCardArrayAdapter.setInitialDelayMillis(500);
            if (listView != null) {
                listView.setExternalAdapter(animCardArrayAdapter, mCardArrayAdapter);
            }

        }
    }




    public class MaterialCard extends Card {

        Context context;
        String mFileName;

        public MaterialCard(Context context,String mFileName) {
            super(context,R.layout.material_card_layout);
            this.context = context;
            this.mFileName = mFileName;
        }

        @Override
        public void setupInnerViewElements(ViewGroup parent, View view) {
            super.setupInnerViewElements(parent, view);
            ImageView img = (ImageView) view.findViewById(R.id.imgFeedMain);
            BurhanTextView txtViewUserName = (BurhanTextView) view.findViewById(R.id.txtUserName);
            BurhanTextView txtViewBrandName = (BurhanTextView) view.findViewById(R.id.txtBrandName);
            BurhanTextView txtViewCountLike = (BurhanTextView) view.findViewById(R.id.txtNoofLikes);
            BurhanTextView txtViewCountComment = (BurhanTextView) view.findViewById(R.id.txtNoofComments);
            BurhanTextView txtViewLike = (BurhanTextView) view.findViewById(R.id.txtLike);
            BurhanTextView txtViewComment = (BurhanTextView) view.findViewById(R.id.txtComment);
            BurhanTextView txtViewShare = (BurhanTextView) view.findViewById(R.id.txtShare);
            SquareImageView profile_image = (SquareImageView) view.findViewById(R.id.image_fb_profile);

            txtViewUserName.setTextSize(getResources().getInteger(R.integer.feed_text_size));
            txtViewBrandName.setTextSize(getResources().getInteger(R.integer.feed_text_size));
            txtViewCountLike.setTextSize(getResources().getInteger(R.integer.feed_text_size));
            txtViewCountComment.setTextSize(getResources().getInteger(R.integer.feed_text_size));
            txtViewLike.setTextSize(getResources().getInteger(R.integer.feed_text_size));
            txtViewLike.setOnClickListener(new txtClickListener());
            txtViewComment.setTextSize(getResources().getInteger(R.integer.feed_text_size));
            txtViewComment.setOnClickListener(new txtClickListener());
            txtViewShare.setTextSize(getResources().getInteger(R.integer.feed_text_size));
            txtViewShare.setOnClickListener(new txtClickListener());

            if (img != null) {
                Picasso.with(context)
                        .load(Uri.parse(mFileName))
                        .fit()
                        .into(img);
            }

            if (profile_image != null) {
                Picasso.with(context)
                        .load(Uri.parse("file:///android_asset/feed/fb_profile.png"))
                        .fit()
                        .into(profile_image);
            }


        }
    }


    class LoaderAsyncTask extends AsyncTask<Void, Void, ArrayList<Card>> {

        LoaderAsyncTask() {
        }

        @Override
        protected ArrayList<Card> doInBackground(Void... params) {
            //elaborate images
            //SystemClock.sleep(1000); //delay to simulate download, don't use it in a real app

            ArrayList<Card> cards = initCard();
            return cards;

        }

        @Override
        protected void onPostExecute(ArrayList<Card> cards) {
            //Update the adapter
            updateAdapter(cards);
            //displayList();
        }
    }


    public class txtClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {

            switch (v.getId()){

                case R.id.txtLike:

                    Toast.makeText(getApplicationContext(),"Like",Toast.LENGTH_LONG).show();

                break;

                case R.id.txtComment:

                    Toast.makeText(getApplicationContext(),"Comment",Toast.LENGTH_LONG).show();

                break;


                case R.id.txtShare:

                    Toast.makeText(getApplicationContext(),"Share",Toast.LENGTH_LONG).show();

                break;

                default:


                break;
            }

        }
    }
}
