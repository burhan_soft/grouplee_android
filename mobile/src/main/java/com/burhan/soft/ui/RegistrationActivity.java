package com.burhan.soft.ui;

import android.os.Bundle;
import android.os.Message;
import android.view.View;

import com.burhan.soft.R;


public class RegistrationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_registration;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeView();
    }

    public void backClick(View view) {
        closeView();
    }

    private void closeView() {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.slide_out_up);
    }
}
