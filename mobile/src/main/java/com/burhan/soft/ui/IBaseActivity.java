package com.burhan.soft.ui;

import android.os.Message;

/**
 * Created by sandip.mahajan on 3/29/2015.
 */
public interface IBaseActivity {

    /**
     * Get layout resource file
     *
     * @return
     */
    int getLayoutResource();

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    void handleMessage(Message message);
}
