package com.burhan.soft.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Message;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.burhan.soft.R;
import com.burhan.soft.adapter.BrandGridAdapter;
import com.burhan.soft.custom.control.BurhanGridView;
import com.burhan.soft.custom.control.BurhanTextView;
import com.burhan.soft.data.BrandGrideItem;

import java.util.ArrayList;


public class BrandsActivity extends BaseActivity {

    BurhanGridView brand_gridView;
    BurhanTextView txtTitle;
    Button btnDone;
    BrandGridAdapter adapter;
    BrandGrideItem item;
    ArrayList<BrandGrideItem> items;
    LayoutInflater inflater;
    LinearLayout ll;
    ArrayList<Integer> pos = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brands);

        brand_gridView = (BurhanGridView) findViewById(R.id.grid_brand);
        btnDone = (Button) findViewById(R.id.btnDone);
        btnDone.setTextSize(TypedValue.COMPLEX_UNIT_PT,5);
        btnDone.setOnClickListener(new clickListener());
        txtTitle = (BurhanTextView)findViewById(R.id.txtTitle);
        txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PT,7);
//        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        items =  new ArrayList<BrandGrideItem>();
        item = new BrandGrideItem(R.drawable.icon_one);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_two);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_three);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_four);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_five);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_six);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_seven);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_eight);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_nine);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_ten);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_eleven);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_twelve);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_thirteen);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_fourteen);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_fifteen);
        items.add(item);

        adapter = new BrandGridAdapter(getApplicationContext());
        adapter.setData(items);
//        View headerView = inflater.inflate(R.layout.refresh_header_view,null);
//                brand_gridView.addHeaderView(headerView);
//        ll =  (LinearLayout) headerView.findViewById(R.id.headerlayout);
//        Button btnCancel = (Button) headerView.findViewById(R.id.btnCancel);
//        btnCancel.setOnClickListener(new clickListener());
        brand_gridView.setAdapter(adapter);

        brand_gridView.setOnItemClickListener(new Listener());
    }

    class clickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(BrandsActivity.this,UserActivity.class);
            startActivity(intent);
        }
    }

    class Listener implements AdapterView.OnItemClickListener{


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            ImageView img = (ImageView) view.findViewById(R.id.imageViewBrand);

            imageSelecter(img,items.get(position).getImageResId(),position);

        }
    }


    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_brands;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }


    protected void imageSelecter(ImageView imageView,int imageResId,int position){

        if(!pos.contains(position)){
            pos.add(position);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imageResId);
        /* set other image top of the first icon */
        Bitmap bitmapStar = BitmapFactory.decodeResource(getResources(), R.drawable.select);

        Bitmap bmOverlay = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawARGB(0x00, 0, 0, 0);
        canvas.drawBitmap(bitmap, 0, 0, null);
        canvas.drawBitmap(bitmapStar, 0, 0, null);

        BitmapDrawable dr = new BitmapDrawable(bmOverlay);
        dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());

        imageView.setImageDrawable(dr);

        }else{
            imageView.setImageResource(imageResId);
            int index = pos.indexOf(position);
            pos.remove(index
            );
        }
    }

 }
