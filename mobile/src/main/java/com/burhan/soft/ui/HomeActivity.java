package com.burhan.soft.ui;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextSwitcher;

import com.burhan.soft.R;
import com.burhan.soft.adapter.CarousalAdapter;
import com.burhan.soft.adapter.GridAdapter;
import com.burhan.soft.data.CarousalVO;
import com.burhan.soft.data.GridItem;

import java.util.ArrayList;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;


public class HomeActivity extends BaseActivity {

    private FeatureCoverFlow mCoverFlow;
    private GridView gridView;
    private CarousalAdapter mAdapter;
    private GridAdapter gridAdapter;
    private ArrayList<CarousalVO> mData = new ArrayList<>(0);
    private ArrayList<GridItem> gridData = new ArrayList<>(0);
    private TextSwitcher mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mData.add(new CarousalVO(R.drawable.carousal_3, "Electronics"));
        mData.add(new CarousalVO(R.drawable.grid_1, "Cloth Shop"));
        mData.add(new CarousalVO(R.drawable.carousal_2, "Store"));
        mData.add(new CarousalVO(R.drawable.grid_2, "Cloth Shop"));
        mData.add(new CarousalVO(R.drawable.carousal_3, "Cloth Shop"));
        mData.add(new CarousalVO(R.drawable.grid_3, "Cloth Shop"));

        mAdapter = new CarousalAdapter(this);
        mAdapter.setData(mData);
        mCoverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        mCoverFlow.setAdapter(mAdapter);

        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        gridData.add(new GridItem(R.drawable.grid_1, "KFC"));
        gridData.add(new GridItem(R.drawable.grid_2, "SHRIMZ"));
        gridData.add(new GridItem(R.drawable.grid_3, "COFFEE"));
        gridData.add(new GridItem(R.drawable.grid_1, "KFC"));
        gridData.add(new GridItem(R.drawable.grid_1, "KFC"));
        gridData.add(new GridItem(R.drawable.grid_2, "SHRIMZ"));
        gridData.add(new GridItem(R.drawable.grid_3, "COFFEE"));
        gridData.add(new GridItem(R.drawable.grid_1, "KFC"));
        gridData.add(new GridItem(R.drawable.grid_1, "KFC"));
        gridData.add(new GridItem(R.drawable.grid_2, "SHRIMZ"));
        gridData.add(new GridItem(R.drawable.grid_3, "COFFEE"));
        gridData.add(new GridItem(R.drawable.grid_1, "KFC"));
        gridData.add(new GridItem(R.drawable.grid_1, "KFC"));
        gridData.add(new GridItem(R.drawable.grid_2, "SHRIMZ"));
        gridData.add(new GridItem(R.drawable.grid_3, "COFFEE"));
        gridData.add(new GridItem(R.drawable.grid_1, "KFC"));

        gridAdapter = new GridAdapter(this);
        gridAdapter.setData(gridData);
        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(gridAdapter);

    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_home;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }
}
