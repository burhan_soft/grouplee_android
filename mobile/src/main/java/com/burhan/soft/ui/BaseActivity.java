package com.burhan.soft.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;

import com.burhan.soft.R;

/**
 * Created by sandip.mahajan on 3/29/2015.
 */
public abstract class BaseActivity extends ActionBarActivity implements IBaseActivity {

    public boolean isLoggedIn;

    /**
     * Navigation to specific UI screen
     *
     * @param tClass
     */
    public void navigateUI(Class tClass) {
        Intent intent = new Intent(this, tClass);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
    }
}
