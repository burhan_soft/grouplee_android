package com.burhan.soft.ui;

import android.content.Intent;
import android.os.Bundle;

import com.burhan.soft.R;
import com.burhan.soft.util.UIUtil;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.plus.PlusClient;

/**
 * Created by sandip.mahajan on 3/29/2015.
 */
public class LoginActionBaseActivity extends PlusBaseActivity {

    public static boolean isAlreadyInitialised;
    public static boolean isLoggedIn;
    public static CallbackManager callbackManager;
    private static AccessToken accessToken;
    private static AccessTokenTracker accessTokenTracker;

    /**
     * Navigation to specific UI screen
     *
     * @param tClass
     */
    public void navigateUI(Class tClass) {
        Intent intent = new Intent(this, tClass);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (!isAlreadyInitialised) {
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager = CallbackManager.Factory.create();
            FacebookSdk.sdkInitialize(getApplicationContext());
            accessTokenTracker = new AccessTokenTracker() {
                @Override
                protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
                    accessToken = newAccessToken;
                    if (isFacebookUserLogin(newAccessToken)) {
                        onClientSignIn();
                    }
                }
            };
        }
        super.onCreate(savedInstanceState);
        if (!isAlreadyInitialised) {
            // Initialize the PlusClient connection.
            // Scopes indicate the information about the user your application will be able to access.
            mPlusClient =
                    new PlusClient.Builder(this, this, this).setScopes(Scopes.PLUS_LOGIN,
                            Scopes.PLUS_ME).build();
        }
        isAlreadyInitialised = true;
    }

    private boolean isFacebookUserLogin(AccessToken currentAccessToken) {
        return currentAccessToken != null;
    }

    public void signOutClient() {
        if (accessToken != null) {
            LoginManager.getInstance().logOut();
            onClientSignOut();
        } else {
            signOut();
            onClientSignOut();
        }
    }

    /**
     * Called when the {@link com.google.android.gms.plus.PlusClient} revokes access to this app.
     */
    @Override
    protected void onPlusClientRevokeAccess() {

    }

    /**
     * Called when the PlusClient is successfully connected.
     */
    @Override
    protected void onClientSignIn() {
        if (!isLoggedIn) {
            isLoggedIn = true;
            if(null != UIUtil.mainActivity) {
                UIUtil.mainActivity.finish();
                UIUtil.mainActivity = null;
            }
            navigateUI(MenuMainActivity.class);
        }
    }

    /**
     * Called when the {@link com.google.android.gms.plus.PlusClient} is disconnected.
     */
    @Override
    protected void onClientSignOut() {
        if(null != UIUtil.menuMainActivity) {
            UIUtil.menuMainActivity.finish();
            UIUtil.menuMainActivity = null;
        }
        navigateUI(MainActivity.class);
    }

    /**
     * Called when the {@link com.google.android.gms.plus.PlusClient} is blocking the UI.  If you have a progress bar widget,
     * this tells you when to show or hide it.
     *
     * @param show
     */
    @Override
    protected void onPlusClientBlockingUI(boolean show) {
        if (show) {
            UIUtil.showProgress(this);
        } else {
            UIUtil.hideProgress();
        }
    }

    /**
     * Called when there is a change in connection state.  If you have "Sign in"/ "Connect",
     * "Sign out"/ "Disconnect", or "Revoke access" buttons, this lets you know when their states
     * need to be updated.
     */
    @Override
    protected void updateConnectButtonState() {

    }
}
