package com.burhan.soft.ui;

import android.os.Bundle;
import android.os.Message;
import android.view.View;

import com.burhan.soft.R;
import com.burhan.soft.custom.control.Fader;


public class MoreActivity extends LoginActionBaseActivity implements IBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_more;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }

    public void logOut(View view) {
        signOutClient();
        Fader.runAlphaAnimation(this, R.id.log_out_button, false);
    }
}
