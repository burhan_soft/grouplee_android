package com.burhan.soft.ui;

import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.burhan.soft.R;
import com.burhan.soft.custom.control.Fader;
import com.burhan.soft.util.UIUtil;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;


public class MainActivity extends LoginActionBaseActivity implements IBaseActivity {

    private LoginButton fbLoginButton;
    private ImageButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        TransitionDrawable transition = (TransitionDrawable) getResources().getDrawable(R.drawable.expand_collapse);
        ImageView logo = (ImageView) findViewById(R.id.main_screen_logo);
        logo.setImageDrawable(transition);
        transition.startTransition(5000);
        fbLoginButton = (LoginButton) findViewById(R.id.authButton);
        loginButton = (ImageButton) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new listener());
        UIUtil.mainActivity = this;
    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_main;
    }


    public class listener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this,LocateMeActivity.class);
            startActivity(intent);
        }
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void showRegistrationPage(View view) {
        Fader.runAlphaAnimation(this, R.id.registration_button, true);
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }

    public void googlePlusSignIn(View view) {
        Fader.runAlphaAnimation(this, R.id.google_plus_button, true);
        signIn();
    }

    public void facebookSignIn(View view) {
        Fader.runAlphaAnimation(this, R.id.facebook_button, true);
        // Callback registration
        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                onClientSignIn();
                finish();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
        fbLoginButton.performClick();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
