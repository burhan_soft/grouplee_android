package com.burhan.soft.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.burhan.soft.R;


public class LocateMeActivity extends BaseActivity {

    EditText edtSearch;
    ImageButton btn_locate_me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locate_me);

        edtSearch = (EditText)findViewById(R.id.edtSearchCity);
        btn_locate_me = (ImageButton)findViewById(R.id.locate_me_button);


        btn_locate_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LocateMeActivity.this,BrandsActivity.class);
                startActivity(intent);
            }
        });
    }



    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_locate_me;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }
}
