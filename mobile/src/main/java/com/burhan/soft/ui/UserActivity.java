package com.burhan.soft.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.burhan.soft.R;
import com.burhan.soft.adapter.ViewPagerAdapter;


/**
 * Created by TAYEBT on 4/21/2015.
 */
public class UserActivity extends FragmentActivity {
    ViewPagerAdapter adapterViewPager;
    ViewPager vpPager;
    Button btnDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_activity);
        vpPager = (ViewPager) findViewById(R.id.vpPager);
        adapterViewPager = new ViewPagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        vpPager.setSoundEffectsEnabled(true);

        btnDone = (Button)findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new clickListener());

        // Attach the page change listener inside the activity
        vpPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            // This method will be invoked when a new page becomes selected.
            @Override
            public void onPageSelected(int position) {
//                Toast.makeText(HomeActivity.this,
//                        "Selected page position: " + position, Toast.LENGTH_SHORT).show();
            }

            // This method will be invoked when the current page is scrolled
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Code goes here
            }

            // Called when the scroll state changes:
            // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
            @Override
            public void onPageScrollStateChanged(int state) {
                // Code goes here
            }
        });
    }

    class clickListener implements View.OnClickListener{


        @Override
        public void onClick(View v) {
            Intent intent = new Intent(UserActivity.this,AlertsActivity.class);
            startActivity(intent);
        }
    }


}