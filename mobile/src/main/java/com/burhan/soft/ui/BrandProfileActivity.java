package com.burhan.soft.ui;

import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;

import com.burhan.soft.R;
import com.squareup.picasso.Picasso;

/**
 * Created by TAYEBT on 5/8/2015.
 */
public class BrandProfileActivity extends BaseActivity{

    ImageView iv_mainBG,iv_logoBG,iv_lineBG,iv_bottomBG,
            iv_icon_followers,iv_icon_check_in,iv_icon_category,iv_icon_finder;
    TextView txt_followBG,txt_title_followers,txt_title_check_in,txt_title_category,txt_title_finder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

        iv_mainBG = (ImageView) findViewById(R.id.iv_brand_frame_brand_image);
        iv_logoBG = (ImageView) findViewById(R.id.iv_brand_frame_logo_image);
        txt_followBG = (TextView) findViewById(R.id.iv_brand_frame_follow_image);
        iv_lineBG = (ImageView) findViewById(R.id.iv_brand_frame_line_image);
        iv_bottomBG = (ImageView) findViewById(R.id.iv_brand_frame_bottom_image);
        iv_icon_followers = (ImageView) findViewById(R.id.brand_prfile_tab_icon_followers);
        iv_icon_check_in = (ImageView) findViewById(R.id.brand_prfile_tab_icon_checkIn);
        iv_icon_category = (ImageView) findViewById(R.id.brand_prfile_tab_icon_category);
        iv_icon_finder = (ImageView) findViewById(R.id.brand_prfile_tab_icon_finder);
        txt_title_followers = (TextView) findViewById(R.id.brand_prfile_tab_title_followers);
        txt_title_check_in = (TextView) findViewById(R.id.brand_prfile_tab_title_checkIn);
        txt_title_category = (TextView) findViewById(R.id.brand_prfile_tab_title_category);
        txt_title_finder = (TextView) findViewById(R.id.brand_prfile_tab_title_finder);

        setImage(iv_mainBG,"file:///android_asset/brand_profile/bg_img.jpg");
        setImage(iv_logoBG,"file:///android_asset/brand_profile/mcdonald_logo.png");
//        setImage(iv_followBG,"file:///android_asset/brand_profile/follow_box.png");
        setImage(iv_lineBG,"file:///android_asset/brand_profile/logo_line.png");
        setImage(iv_bottomBG,"file:///android_asset/brand_profile/kfc_flyers.jpg");
        setImage(iv_icon_followers,"file:///android_asset/brand_profile/followers_icon.png");
        setImage(iv_icon_check_in,"file:///android_asset/brand_profile/checkin_icon.png");
        setImage(iv_icon_category,"file:///android_asset/brand_profile/category_icon.png");
        setImage(iv_icon_finder,"file:///android_asset/brand_profile/store_finder_icon.png");
        txt_title_followers.setText("255 Followers");
        txt_title_check_in.setText("Check In");
        txt_title_category.setText("Category");
        txt_title_finder.setText("Store Finder");
        txt_followBG.setText("FOLLOW");
        txt_followBG.setBackground(getResources().getDrawable(R.drawable.follow_box));
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_brand_profile;
    }

    @Override
    public void handleMessage(Message message) {

    }

    void setImage(ImageView imageView,String path){
        if (imageView != null) {
            Picasso.with(getApplicationContext())
                    .load(Uri.parse(path))
                    .fit()
                    .into(imageView);
        }
    }
}
