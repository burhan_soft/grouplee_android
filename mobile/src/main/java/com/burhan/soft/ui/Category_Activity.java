package com.burhan.soft.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.burhan.soft.R;
import com.burhan.soft.adapter.BrandGridAdapter;
import com.burhan.soft.custom.control.BurhanGridView;
import com.burhan.soft.data.BrandGrideItem;

import java.util.ArrayList;

/**
 * Created by TAYEBT on 4/21/2015.
 */
public class Category_Activity extends Fragment {

    BurhanGridView brand_gridView;
    BrandGridAdapter adapter;
    BrandGrideItem item;
    ArrayList<BrandGrideItem> items;


    // Store instance variables
    private String title;
    private int page;

    // newInstance constructor for creating fragment with arguments
    public static Category_Activity newInstance(int page, String title) {
        Category_Activity fragmentAccessories = new Category_Activity();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentAccessories.setArguments(args);
        return fragmentAccessories;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_category, container, false);
        brand_gridView = (BurhanGridView) view.findViewById(R.id.category_dridView);


        items =  new ArrayList<BrandGrideItem>();
        item = new BrandGrideItem(R.drawable.icon_one);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_two);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_three);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_four);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_five);
        items.add(item);
        item = new BrandGrideItem(R.drawable.icon_six);
        items.add(item);


        adapter = new BrandGridAdapter(getActivity());
        adapter.setData(items);
//        View headerView = inflater.inflate(R.layout.refresh_header_view,null);
//                brand_gridView.addHeaderView(headerView);
//        ll =  (LinearLayout) headerView.findViewById(R.id.headerlayout);
//        Button btnCancel = (Button) headerView.findViewById(R.id.btnCancel);
//        btnCancel.setOnClickListener(new clickListener());
        brand_gridView.setAdapter(adapter);

        return view;
    }
}