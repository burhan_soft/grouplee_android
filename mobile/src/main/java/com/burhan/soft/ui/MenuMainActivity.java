package com.burhan.soft.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.burhan.soft.R;
import com.burhan.soft.util.UIUtil;


public class MenuMainActivity extends TabActivity {

    // Tab host to hold the activities
    private TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_menu_main);
        tabHost = getTabHost();
        setTabs();
        tabHost.setCurrentTab(2);
        UIUtil.menuMainActivity = this;
    }

    /**
     * Set tab in tab activity
     */
    private void setTabs() {
        addTab("Brands", R.drawable.tab_brands, BrandsActivity.class);
        addTab("My Bag", R.drawable.tab_mybag, MyBagActivity.class);
        addTab("Home", R.drawable.tab_home, HomeActivity.class);
        addTab("Alerts", R.drawable.tab_alert, AlertsActivity.class);
        addTab("", R.drawable.tab_more, MoreActivity.class);
    }

    /**
     * Add tab with tab image resource id and tab name
     *
     * @param tabName
     * @param imageName
     * @param childClass
     */
    private void addTab(String tabName, int imageName, Class<?> childClass) {
        Intent intent = new Intent(this, childClass);
        TabHost.TabSpec spec = tabHost.newTabSpec("tab" + tabName);

        View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, getTabWidget(), false);
        TextView title = (TextView) tabIndicator.findViewById(R.id.title);
        ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
        if ("".equalsIgnoreCase(tabName)) {
            title.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) icon.getLayoutParams();
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            icon.setLayoutParams(layoutParams);
        }
        title.setText(tabName);
        icon.setImageResource(imageName);
        spec.setIndicator(tabIndicator);
        spec.setContent(intent);
        tabHost.addTab(spec);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
