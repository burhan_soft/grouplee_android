package com.burhan.soft.ui;

import android.os.Bundle;
import android.os.Message;

import com.burhan.soft.R;
import com.burhan.soft.adapter.AlertsAdapter;
import com.burhan.soft.data.AlertsItem;
import com.nirhart.parallaxscroll.views.ParallaxListView;

import java.util.ArrayList;


public class AlertsActivity extends BaseActivity {

    ParallaxListView listView;
    AlertsAdapter adapter;
    ArrayList<AlertsItem> items = new ArrayList<AlertsItem>();
    AlertsItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

        listView = (ParallaxListView)findViewById(R.id.alert_listview);

        item = new AlertsItem(R.drawable.icon_one,"Posted new Flyer..","06.30. 01-04-2015");
        items.add(item);

        item = new AlertsItem(R.drawable.icon_two,"Open a new Store..","07.30. 01-04-2015");
        items.add(item);

        item = new AlertsItem(R.drawable.icon_three,"New Arrivals..","07.00. 01-04-2015");
        items.add(item);

        adapter = new AlertsAdapter(getApplicationContext());
        adapter.sendData(items);

        listView.setAdapter(adapter);

    }

    /**
     * Get layout resource file
     *
     * @return
     */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_alerts;
    }

    /**
     * Handle messages received to the UI elements
     *
     * @param message
     */
    @Override
    public void handleMessage(Message message) {

    }


}
