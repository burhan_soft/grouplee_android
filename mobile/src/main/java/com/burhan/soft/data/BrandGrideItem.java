package com.burhan.soft.data;

/**
 * Created by TAYEBT on 4/17/2015.
 */
public class BrandGrideItem {

    private int imageResId;

    public BrandGrideItem(int imageResId) {
        this.imageResId = imageResId;
    }


    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }
}
