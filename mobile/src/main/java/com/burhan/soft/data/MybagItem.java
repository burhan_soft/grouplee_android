package com.burhan.soft.data;

/**
 * Created by TAYEBT on 4/24/2015.
 */
public class MybagItem {

    private String title;
    String [] imgList;

    public MybagItem(String title, String [] imgList) {
        this.title = title;
        this.imgList = imgList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getImgList() {
        return imgList;
    }

    public void setImgList(String[] imgList) {
        this.imgList = imgList;
    }
}
