package com.burhan.soft.data;

import com.burhan.soft.custom.control.SquareImageView;

/**
 * Created by TAYEBT on 4/27/2015.
 */
public class FeedItem {

    SquareImageView user_profile;
    private String username;
    private String brand;
    private String countLikes;
    private String countComments;

    public FeedItem(SquareImageView user_profile,String username,String brand,
                    String countLikes,String countComments) {

        this.user_profile = user_profile;
        this.username = username;
        this.brand = brand;
        this.countLikes = countLikes;
        this.countComments = countComments;

    }


    public SquareImageView getUser_profile() {
        return user_profile;
    }

    public void setUser_profile(SquareImageView user_profile) {
        this.user_profile = user_profile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCountLikes() {
        return countLikes;
    }

    public void setCountLikes(String countLikes) {
        this.countLikes = countLikes;
    }

    public String getCountComments() {
        return countComments;
    }

    public void setCountComments(String countComments) {
        this.countComments = countComments;
    }
}
