package com.burhan.soft.data;

/**
 * Created by TAYEBT on 4/24/2015.
 */
public class ExpandableListItem {

    private String imageResId;
    private String title;

    public ExpandableListItem(String imageResId, String title) {
        this.imageResId = imageResId;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageResId() {

        return imageResId;
    }

    public void setImageResId(String imageResId) {
        this.imageResId = imageResId;
    }
}
