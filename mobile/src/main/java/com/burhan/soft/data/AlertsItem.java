package com.burhan.soft.data;

/**
 * Created by TAYEBT on 4/21/2015.
 */
public class AlertsItem {

    private int imageResId;
    private String title;
    private String date_and_time;

    public AlertsItem(int imageResId,String title,String date_and_time) {
        this.imageResId = imageResId;
        this.title = title;
        this.date_and_time = date_and_time;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate_and_time() {
        return date_and_time;
    }

    public void setDate_and_time(String date_and_time) {
        this.date_and_time = date_and_time;
    }
}
