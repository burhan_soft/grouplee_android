package com.burhan.soft.data;

/**
 * Created by sandip.mahajan on 3/28/2015.
 */
public class GridItem {

    private int imageResId;
    private String title;

    public GridItem(int imageResId, String title) {
        this.imageResId = imageResId;
        this.title = title;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
