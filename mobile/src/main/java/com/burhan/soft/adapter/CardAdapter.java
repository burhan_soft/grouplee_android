package com.burhan.soft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.burhan.soft.R;
import com.burhan.soft.data.MybagItem;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardView;

/**
 * Created by TAYEBT on 4/24/2015.
 */
public class CardAdapter extends CardArrayAdapter{

    Context context;
    List<Card> cards;
    ArrayList<MybagItem> mybagItems = new ArrayList<MybagItem>();


    public CardAdapter(Context context, List<Card> cards) {
        super(context, cards);
        this.context = context;
        this.cards = cards;
    }

    public void sendData(ArrayList<MybagItem> mybagItems) {

        this.mybagItems = mybagItems;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public Card getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_mybag, null);

            ViewHolder holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.textview_item_mybag);
//            holder.txtTitle.setTextColor(context.getResources().getColor(R.color.black));
//            holder.listView = (ListView) convertView.findViewById(R.id.listview_item_mybag);
//            holder.cardView = (CardView) convertView.findViewById(R.id.row_card);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();

        holder.txtTitle.setText(mybagItems.get(position).getTitle());

//        holder.cardView.setCard(cards.get(position));

//        holder.listView = mybagItems.get(position).getImgList();


        return super.getView(position, convertView, parent);
    }

    public class ViewHolder{

        TextView txtTitle;
        CardView cardView;
    }
}
