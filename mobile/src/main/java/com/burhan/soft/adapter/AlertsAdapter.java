package com.burhan.soft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.burhan.soft.R;
import com.burhan.soft.custom.control.BurhanTextView;
import com.burhan.soft.custom.control.SquareImageView;
import com.burhan.soft.data.AlertsItem;

import java.util.ArrayList;

/**
 * Created by TAYEBT on 4/21/2015.
 */
public class AlertsAdapter extends BaseAdapter{

    Context context;
    ArrayList<AlertsItem> alertsItems = new ArrayList<AlertsItem>();

    public AlertsAdapter(Context context) {

        this.context = context;

    }

    public void sendData(ArrayList<AlertsItem> alerts_data){
            alertsItems = alerts_data;
    }

    @Override
    public int getCount() {
        return alertsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return alertsItems.get(position).toString();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_alerts, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView = (SquareImageView) rowView
                    .findViewById(R.id.imageViewAlerts);
            viewHolder.txtTiltle = (BurhanTextView) rowView.findViewById(R.id.textViewTitle);
            viewHolder.txtTiltle.setTextColor(context.getResources().getColor(R.color.black));
            viewHolder.txtDateAndTime = (BurhanTextView) rowView.findViewById(R.id.textViewDateAndTime);
            viewHolder.txtDateAndTime.setTextColor(context.getResources().getColor(R.color.black));
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        holder.imageView.setImageResource(alertsItems.get(position).getImageResId());
        holder.txtTiltle.setText(alertsItems.get(position).getTitle());
        holder.txtDateAndTime.setText(alertsItems.get(position).getDate_and_time());

        return rowView;
    }


    public class ViewHolder{

        SquareImageView imageView;
        BurhanTextView txtTiltle;
        BurhanTextView txtDateAndTime;
    }
}
