package com.burhan.soft.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.burhan.soft.ui.Category_Activity;
import com.burhan.soft.ui.FollowingActivity;

/**
 * Created by TAYEBT on 4/21/2015.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2;
    private String [] pageTitle = {"FOLLOWING","CATEGORIES"};

    public ViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return FollowingActivity.newInstance(0, "Page # 1");
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return Category_Activity.newInstance(1, "Page # 2");
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return pageTitle[position].toString();
    }

}
