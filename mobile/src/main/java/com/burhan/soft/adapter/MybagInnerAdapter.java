package com.burhan.soft.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.burhan.soft.R;
import com.squareup.picasso.Picasso;

/**
 * Created by TAYEBT on 4/25/2015.
 */
public class MybagInnerAdapter extends BaseAdapter {

    Context context;
    String [] list;

    public MybagInnerAdapter(Context context,String [] mFileName) {
        this.context = context;
        this.list = mFileName;

    }

    @Override
    public int getCount() {
        return list.length;
    }

    @Override

    public Object getItem(int position) {
        return list[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_mybag_inner, null);

            ViewHolder holder = new ViewHolder();
            holder.imageView = (ImageView) rowView.findViewById(R.id.mybaginnerImageView);
            rowView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        if (holder.imageView != null) {
            Picasso.with(context)
                    .load(Uri.parse(list[position]))
                    .fit()
                    .into(holder.imageView);
        }

        return rowView;
    }

    public class ViewHolder{

        ImageView imageView;
    }
}

