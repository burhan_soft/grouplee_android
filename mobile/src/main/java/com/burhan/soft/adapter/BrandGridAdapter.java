package com.burhan.soft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.burhan.soft.R;
import com.burhan.soft.data.BrandGrideItem;

import java.util.ArrayList;

/**
 * Created by TAYEBT on 4/17/2015.
 */
public class BrandGridAdapter extends BaseAdapter {

    Context context;
    ArrayList<BrandGrideItem> item = new ArrayList<BrandGrideItem>();

    public BrandGridAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<BrandGrideItem> images){

        item = images;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int position) {
        return item.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.brand_grid_item, null);

            Holder viewHolder = new Holder();
            viewHolder.imageView = (ImageView) rowView
                    .findViewById(R.id.imageViewBrand);
            rowView.setTag(viewHolder);
        }

        Holder holder = (Holder) rowView.getTag();

        holder.imageView.setImageResource(item.get(position).getImageResId());

        return rowView;
    }


    class Holder{
        ImageView imageView;
    }
}
