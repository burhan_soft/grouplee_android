package com.burhan.soft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.burhan.soft.R;
import com.burhan.soft.data.CarousalVO;
import com.burhan.soft.data.GridItem;

import java.util.ArrayList;

/**
 * Created by sandip.mahajan on 3/28/2015.
 */
public class GridAdapter extends BaseAdapter {

    private ArrayList<GridItem> mData = new ArrayList<>(0);
    private Context mContext;

    public GridAdapter(Context context) {
        mContext = context;
    }

    public void setData(ArrayList<GridItem> data) {
        mData = data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int pos) {
        return mData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.grid_item, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.text);
            viewHolder.image = (ImageView) rowView
                    .findViewById(R.id.picture);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        holder.image.setImageResource(mData.get(position).getImageResId());
        holder.text.setText(mData.get(position).getTitle());

        return rowView;
    }


    static class ViewHolder {
        public TextView text;
        public ImageView image;
    }
}
