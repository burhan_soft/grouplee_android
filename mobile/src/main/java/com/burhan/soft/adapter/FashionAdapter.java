package com.burhan.soft.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.burhan.soft.R;
import com.burhan.soft.data.BrandGrideItem;

import java.util.ArrayList;

/**
 * Created by TAYEBT on 4/22/2015.
 */
public class FashionAdapter extends BaseAdapter {

    Context context;
    ArrayList<BrandGrideItem> items = new ArrayList<BrandGrideItem>();

    public FashionAdapter(Context context) {
        this.context = context;
    }

    public void sendData(ArrayList<BrandGrideItem> list){
        items = list;
    }

    @Override
    public int getCount() {
        return items.size();
    }



    @Override
    public Object getItem(int position) {

        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_listview_fashion, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) rowView
                    .findViewById(R.id.imgFashionList);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        holder.imageView.setImageResource(items.get(position).getImageResId());

        return rowView;
    }


    public class ViewHolder{

        ImageView imageView;
    }
}
