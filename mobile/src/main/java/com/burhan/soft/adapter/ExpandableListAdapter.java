package com.burhan.soft.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.burhan.soft.R;
import com.burhan.soft.data.ExpandableListItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by TAYEBT on 4/24/2015.
 */
public class ExpandableListAdapter extends BaseAdapter {

    Context context;
ArrayList<ExpandableListItem> items = new ArrayList<ExpandableListItem>();

    public ExpandableListAdapter(Context context,ArrayList<ExpandableListItem> itemsList) {

        this.context = context;
        this.items = itemsList;

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.carddemo_expand_list_item, null);

            ViewHolder holder = new ViewHolder();
            holder.imageView = (ImageView) rowView
                    .findViewById(R.id.expan_list_imageview);
            holder.txtTiltle = (TextView) rowView.findViewById(R.id.expandcard_list_textview);
            rowView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        //You could use a singleton instance of picasso!
        if (holder.imageView != null) {
            Picasso.with(context)
                    .load(Uri.parse(items.get(position).getImageResId()))
                    .fit()
                    .into(holder.imageView);
        }

        holder.txtTiltle.setText(items.get(position).getTitle());

        return rowView;
    }


    public class ViewHolder{

        ImageView imageView;
        TextView txtTiltle;
    }
}
